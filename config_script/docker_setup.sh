git config --global user.name "Jules Graeff"
git config --global user.email jules.graeff@gmail.com

# DOCKER INSTALLATION

apt-get install curl apt-transport-https ca-certificates software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt update
apt-cache policy docker-ce
apt install docker-ce

# fix permission

groupadd docker
usermod -aG docker $USER
# newgrp docker

# DOCKER COMPOSE INSTALLATION

curl -L "https://github.com/docker/compose/releases/download/1.26.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

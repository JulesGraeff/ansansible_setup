# Ansible setup

Ansible test script which setting the PC local config.
*Ansible is an automation tool that allows to configure device (remotely or locally)*

## Dependencies

- First you have to download all necessary minimum packages (if not already done)

```bash
sudo apt-get update
sudo apt-get install openssh-server
ssh-keygen
sudo apt install git
git config --global user.email "your_email@gmail.com"
git config --global user.name "Your Name"
```

Then setup your ssh key on your Gitlab account.

- Install Ansible

```bash
sudo apt install software-properties-common
sudo add-apt-repository --yes --update ppa:ansible/ansible
sudo apt install ansible
```

## Install ansible role dependencies

...
How to add submodules automatically

"git submodule update --init --recursive" doesn't seems to work.

Blocker with role access ... 

While it's fixed used following command lines to set up code:
```bash
sudo apt install software-properties-common apt-transport-https wget
wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
sudo apt install code
```

## How to run

```bash
ansible-playbook local.yml
```

Unfortunately, "oh-my-zsh" is not set correctly via ansible for now so please run the following command after :

```bash
./config-script/oh-zsh-setup.sh
```

Then reboot to deploy the zsh config.